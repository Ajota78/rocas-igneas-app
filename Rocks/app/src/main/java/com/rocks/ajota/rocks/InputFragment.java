package com.rocks.ajota.rocks;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.rocks.ajota.rocks.model.roca;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.zip.CheckedInputStream;


public class InputFragment extends Fragment  {
    public ArrayList<roca> rocas;
    String textura="Fanerítica";
    int ortoclasa=0;
    int plagioclasa=0;
    int cuarzo=0;
    String tipo_plagioclasa="";
    int count=0;
    int count2=0;
    public int imagen;
    public Spinner spinner_Texturas;
    public Spinner spinner_porfidica;
    public Spinner spinner_plagioclasa;
    public CheckBox checkBox_plagioclasa;
    public String[] texturaslist;
    public String[] porfidicalist;
    public String[] plagioclasalist;
    public InputFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        count=0;
        count2=0;
        texturaslist= getResources().getStringArray(R.array.Texturas);
        porfidicalist = getResources().getStringArray(R.array.porfidica);
        plagioclasalist = getResources().getStringArray(R.array.plagioclasa);

        final View mview = inflater.inflate(R.layout.fragment_input, container, false);
        spinner_Texturas = (Spinner)mview.findViewById(R.id.spinner_texturas);
        final LinearLayout layout_porfidica= (LinearLayout) mview.findViewById(R.id.linearlayout_porfidica);
        final LinearLayout layout_textura_plagioclasa= (LinearLayout) mview.findViewById(R.id.linearlayout_texturas_plagioclasa);

        spinner_porfidica=(Spinner)mview.findViewById(R.id.spinner_texturas_porfidica);
        spinner_plagioclasa=(Spinner)mview.findViewById(R.id.spinner_texturas_plagioclasa);
        checkBox_plagioclasa=(CheckBox)mview.findViewById(R.id.checkbox_plagioclasa);
        final EditText editText_ortoclasa=(EditText)mview.findViewById(R.id.ortoclasa);
        final EditText editText_plagioclasa=(EditText)mview.findViewById(R.id.plagioclasa);
        final EditText editText_cuarzo=(EditText)mview.findViewById(R.id.cuarzo);
        final Button button_buscar =(Button) mview.findViewById(R.id.button_buscar);

        spinner_Texturas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                GetText(position);
                if(textura.equals(texturaslist[1])){
                    layout_porfidica.setVisibility(View.VISIBLE);
                    textura=porfidicalist[spinner_porfidica.getSelectedItemPosition()];
                }else{
                    layout_porfidica.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_porfidica.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                GetText2(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_plagioclasa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                GetText3(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        editText_ortoclasa.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if((event.getAction()== KeyEvent.ACTION_DOWN )&&(keyCode == KeyEvent.KEYCODE_ENTER)) {
                    try{
                    ortoclasa = Integer.parseInt(String.valueOf(editText_ortoclasa.getText()));
                }catch (Exception e){}

                InputMethodManager inputMethodManager =  (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(mview.getWindowToken(), 0);
                    if((plagioclasa>=66)&&(ortoclasa<10)){
                        layout_textura_plagioclasa.setVisibility(View.VISIBLE);
                        tipo_plagioclasa= plagioclasalist[spinner_plagioclasa.getSelectedItemPosition()];
                    }else{
                        layout_textura_plagioclasa.setVisibility(View.GONE);
                        tipo_plagioclasa="";
                    }

                }
                return false;
            }
        });
        editText_plagioclasa.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if((event.getAction()== KeyEvent.ACTION_DOWN )&&(keyCode == KeyEvent.KEYCODE_ENTER)) {
                    try {
                        plagioclasa = Integer.parseInt(String.valueOf(editText_plagioclasa.getText()));
                    }catch (Exception e){}
                    if(editText_plagioclasa.getText().equals("")){
                        plagioclasa=0;
                    }
                    if((plagioclasa>=66)&&(ortoclasa<10)){
                        layout_textura_plagioclasa.setVisibility(View.VISIBLE);
                    }else{
                        layout_textura_plagioclasa.setVisibility(View.GONE);
                        tipo_plagioclasa="";
                    }

                }
                return false;
            }
        });
        checkBox_plagioclasa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkBox_plagioclasa.isChecked()==true){
                    plagioclasa=0;
                }
            }
        });

        editText_cuarzo.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if((event.getAction()== KeyEvent.ACTION_DOWN )&&(keyCode == KeyEvent.KEYCODE_ENTER)) {
                    cuarzo= Integer.parseInt(String.valueOf(editText_cuarzo.getText()));
                    InputMethodManager inputMethodManager =  (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(mview.getWindowToken(), 0);
                }
                return false;
            }
        });

        button_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombreroca="";


                if(plagioclasa>=66){
                    if(ortoclasa<10) {
                        if (tipo_plagioclasa.equals("Plagioclasa Na")) {
                            if(textura.equals("Fanerítica")) {
                                if (cuarzo < 10) {
                                    nombreroca="Diorita";
                                    imagen=R.drawable.diorita;
                                } else {
                                     nombreroca="Diorita de cuarzo";
                                    imagen=R.drawable.diorita_de_cuarzo;
                                }
                            }if(textura.equals("Porfírica faneritica")) {
                                if (cuarzo < 10) {
                                     nombreroca=" Diorita porfirica";
                                    imagen=R.drawable.diorita_porfirica;
                                } else {
                                    nombreroca=" Diorita de cuarzo porfirica";
                                    imagen=R.drawable.diorita_de_cuarzo_porfirica;
                                }
                            }
                            if(textura.equals("Porfírica afanitica")) {
                                if (cuarzo < 10) {
                                    nombreroca=" Andesita porfirica";
                                    imagen=R.drawable.andesita_porfirica;
                                } else {
                                    nombreroca=" Dacita porfirica";
                                    imagen=R.drawable.dacita_porfirica;
                                }
                            }
                            if(textura.equals("Afanítica")) {
                                if (cuarzo < 10) {
                                    nombreroca=" Andesita";
                                    imagen=R.drawable.andesita;

                                } else {
                                    nombreroca=" Dacita";
                                    imagen=R.drawable.dacita;
                                }
                            }
                        }
                        if (tipo_plagioclasa.equals("Plagioclasa Ca")) {
                            if(textura.equals("Fanerítica")) {
                                if (cuarzo < 10) {
                                    nombreroca=" Gabro";
                                    imagen=R.drawable.gabro;
                                } else {
                                    nombreroca=" Sin resultados";
                                    imagen=R.drawable.titulo2;
                                }
                            }if(textura.equals("Porfírica faneritica")) {
                                if (cuarzo < 10) {
                                    nombreroca=" Gabro porfidico";
                                    imagen=R.drawable.gabro_porfirico;
                                } else {
                                    nombreroca=" Sin resultados";
                                    imagen=R.drawable.titulo2;
                                }
                            }
                            if(textura.equals("Porfírica afanitica")) {
                                if (cuarzo < 10) {
                                    nombreroca=" Basalto porfirico";
                                    imagen=R.drawable.basalto_porfirico;
                                } else {
                                    nombreroca=" Sin resultados";
                                    imagen=R.drawable.titulo2;
                                }
                            }
                            if(textura.equals("Afanítica")) {
                                if (cuarzo < 10) {
                                    nombreroca=" Basalto de olivino de analcita";
                                    imagen=R.drawable.basalto_de_olivino_de_analcita;
                                } else {
                                    nombreroca=" Sin resultados";
                                    imagen=R.drawable.titulo2;
                                }
                            }
                        }

                }else if(ortoclasa>=10){
                        if(cuarzo>10){
                            if(textura.equals("Fanerítica")) {
                                nombreroca=" Granodioria";
                                imagen=R.drawable.granodiorita;
                            }if(textura.equals("Porfírica faneritica")) {
                                nombreroca="Granodiorita porfirica";
                                imagen=R.drawable.granito_porfirico;
                            }
                            if(textura.equals("Porfírica afanitica")) {
                                nombreroca="Dacita porfirica";
                                imagen=R.drawable.dacita_porfirica;
                            }
                            if(textura.equals("Afanítica")) {
                                nombreroca="Dacita";
                                imagen=R.drawable.dacita;
                            }
                        }else{
                            nombreroca="Sin resultados";
                            imagen=R.drawable.titulo2;
                        }
                    }
            }else if((ortoclasa>=66)&&checkBox_plagioclasa.isChecked()==true){
                    if(textura.equals("Fanerítica")) {
                        if (cuarzo < 10) {
                            nombreroca=" Sienita";
                            imagen=R.drawable.sienita;
                        } else {
                            nombreroca=" Granito";
                            imagen=R.drawable.granito;
                        }
                    }if(textura.equals("Porfírica faneritica")) {
                        if (cuarzo < 10) {
                            nombreroca=" Sianita porfirica";
                            imagen=R.drawable.sienita_porfirica;
                        } else {
                            nombreroca=" Granito porfirico";
                            imagen=R.drawable.granito_porfirico;
                        }
                    }
                    if(textura.equals("Porfírica afanitica")) {
                        if (cuarzo < 10) {
                            nombreroca=" Traquita porfirica";
                            imagen=R.drawable.traquita_porfirica;
                        } else {
                            nombreroca=" Riolita porfirica";
                            imagen=R.drawable.riolita_porfirica;
                        }
                    }
                    if(textura.equals("Afanítica")) {
                        if (cuarzo < 10) {
                            nombreroca=" Traquita";
                            imagen=R.drawable.traquita;
                        } else {
                            nombreroca=" Riolita";
                            imagen=R.drawable.riolita;
                        }
                    }

                }else if((ortoclasa>33&&ortoclasa<66)&&checkBox_plagioclasa.isChecked()==true) {
                    if (textura.equals("Fanerítica")) {
                        if (cuarzo < 10) {
                            nombreroca=" Monzonita";
                            imagen=R.drawable.monzonita;
                        } else {
                            nombreroca=" Monzonita cuarcifera";
                            imagen=R.drawable.monzonita_cuarcifera;
                        }
                    }
                    if (textura.equals("Porfírica faneritica")) {
                        if (cuarzo < 10) {
                            nombreroca=" Monzonita porfirica";
                            imagen=R.drawable.monzonita_porfirica;
                        } else {
                            nombreroca=" Monzonita cuarcifera porfirica";
                            imagen=R.drawable.monzonita_cuarcifera_porfirica;
                        }
                    }
                    if (textura.equals("Porfírica afanitica")) {
                        if (cuarzo < 10) {
                            nombreroca=" Latita porfirica";
                            imagen=R.drawable.latita_porfirica;
                        } else {
                            nombreroca=" Latita de cuarzo porfirica";
                            imagen=R.drawable.latita_de_cuarzo_porfirica;
                        }
                    }
                    if (textura.equals("Afanítica")) {
                        if (cuarzo < 10) {
                            nombreroca=" Latita traquiandesita";
                            imagen=R.drawable.latita_traquiandesita;
                        } else {
                            nombreroca=" Latita de cuarzo";
                            imagen=R.drawable.latita_de_cuarzo;
                        }
                    }
                }
                if(ortoclasa<33&&plagioclasa==0){
                    nombreroca="Sin resultados";
                    imagen=R.drawable.titulo2;
                }
     //           Toast.makeText(getActivity(),textura+"\n"+ortoclasa+"\n "+plagioclasa+"\n "+cuarzo,Toast.LENGTH_SHORT).show();

    //            Toast.makeText(getActivity(),nombreroca,Toast.LENGTH_SHORT).show();
                Dialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                View mView = inflater.inflate(R.layout.dialog, null);
                TextView nombre_roca=(TextView)mView.findViewById(R.id.nombre_roca);
                ImageView imagen_roca=(ImageView)mView.findViewById(R.id.imagen_roca);
                nombre_roca.setText(nombreroca);
                imagen_roca.setImageResource(imagen);
                builder.setView(mView);
                dialog = builder.create();
                dialog.show();
            }
        });
        return mview;

    }

    private void GetText(int position) {
        Toast.makeText(getActivity(),texturaslist[position],Toast.LENGTH_SHORT).show();
        textura=texturaslist[position];
    }
    private void GetText2(int position) {
        Toast.makeText(getActivity(),porfidicalist[position],Toast.LENGTH_SHORT).show();
        textura= porfidicalist[position];
    }
    private void GetText3(int position) {
        Toast.makeText(getActivity(),plagioclasalist[position],Toast.LENGTH_SHORT).show();
        tipo_plagioclasa= plagioclasalist[position];
    }

    public void RellenarListado(){
        rocas= new ArrayList<>();
        //roca roca1 = new roca("Fanerítica",true,);
       // new roca("Fanerítica",true,)
       // rocas.add()
    }

}
