package com.rocks.ajota.rocks.model;

/**
 * Created by Alvaro-Tabilo on 08-12-2017.
 */

public class roca {
    /*
    * ortoclasa
    * plagioclasa
    * cuarzo
    * textura*/

    public String textura;
    public boolean cuarzo;//mayor al 10% true
    public int ortoclasa;
    public boolean plagoclasa;
    public int imagen;
    public String nombre;
    public String descripcion;
/*
* ortoclasa >2/3 (66)                   2-4
*           [1/3(33)]> x > [2/3(66)]    2-3
*
*           >10                         2
*           <10                         1
*           */

/*
* Cuarzo    >10                          true
*           <10                          false
* */

/*plagioclasa   >2/3(66)                true
*               <2/3                    false
* */
    public roca() {
    }

    public roca(String textura, boolean cuarzo, int ortoclasa, boolean plagoclasa, int imagen,String nombre, String descripcion) {
        this.textura = textura;
        this.cuarzo = cuarzo;
        this.ortoclasa = ortoclasa;
        this.plagoclasa = plagoclasa;
        this.imagen = imagen;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }
}
